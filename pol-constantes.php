<?php
namespace cms\pol\elaborado;


class Constantes { 
      
public function constantes(){

    define ('ABSPATH', __DIR__ . "/");
define ('POLINC', ABSPATH . 'nucleo');

define('POL_DEBUG', false);

define( 'POL_CONTENT_DIR', ABSPATH . 'codigo' );

define( 'POL_PLUGIN_DIR', POL_CONTENT_DIR . '/plugins' );
define( 'POLMU_PLUGIN_DIR', POL_CONTENT_DIR . '/mu-plugins' );

//define( 'POL_CONTENT_URL', $this->get_option( 'siteurl' ) . '/codigo' );

//define( 'POL_PLUGIN_URL', POL_CONTENT_URL . '/plugins' );
//define( 'POLMU_PLUGIN_URL', POL_CONTENT_URL . '/mu-plugins' );

define( 'REST_REQUEST', true );
define( 'XMLRPC_REQUEST', true );
define( 'DOING_AJAX', true );

define( 'WP_INSTALLING', true );
define( 'MULTISITE', false );

define( 'POL_LANG_DIR', POL_CONTENT_DIR . '/languages' );

define( 'WP_USE_THEMES', true );

}



}

?>