<?php
namespace cms\pol\elaborado;

require_once __DIR__ .'pol-query.php';
require_once __DIR__ .'/sitemaps/pol-sitemaps.php';
require_once __DIR__ .'/sitemaps/pol-sitemaps-renderer.php';
require_once __DIR__ .'/sitemaps/pol-sitemaps-stylesheet.php';
require_once __DIR__ .'pol-plugin.php';
require_once __DIR__ .'pol-formatting.php';
require_once __DIR__ .'pol-option.php';
require_once __DIR__ .'pol-load.php';
require_once __DIR__ . 'pol-ms-network.php';
require_once __DIR__ . 'pol-load.php';
require __DIR__ . '/pol-I10n.php';


use cms\pol\elaborado\Query as Query;
use cms\pol\elaborado\SiteMaps as SiteMaps;
use cms\pol\elaborado\Hook as Hook;
use cms\pol\elaborado\Opciones as Opciones;
use cms\pol\elaborado\Formato as Formato;
use cms\pol\elaborado\Sitemaps_Renderer as Sitemaps_Renderer;
use cms\pol\elaborado\Sitemaps_Stylesheet as Sitemaps_Stylesheet;
use cms\pol\elaborado\Traducciones as Traducciones;
use cms\pol\elaborado\Ms_Network as Ms_Network;
use cms\pol\elaborado\Identacion as Identacion;

class Functions{ 

protected $query;
protected $sitemap;
protected $hook;
protected $sitemap_render;
protected $sitemap_style;
protected $format;
protected $opt;
protected $traducir;
protected $ms_networl;
protected $ident;


	public function __construct()
	{

		$this->maybeint = -10;
	    $this->query = new Query();		
		$this->sitemap = new SiteMaps();		
		$this->hook = new Hook();		
		$this->sitemap_render = new Sitemaps_Renderer();		
		$this->sitemap_style = new Sitemaps_Stylesheet();		
		$this->format = new Formato();		
		$this->opt = new Opciones();
		$this->traducir = new Traducciones();
		$this->ms_networl = new Ms_Network();
		$this->ident = new Identacion();
	}


public function template_redirect(){
	

     $this->hook->add_action('template_redirect', 'render_sitemaps');
}

private function render_sitemaps() {
   

    $sitemap         = $this->sanitize_text_field( $this->query->get_query_var( 'sitemap' ) );
    $object_subtype  = $this->sanitize_text_field( $this->query->get_query_var( 'sitemap-subtype' ) );
    $stylesheet_type = $this->sanitize_text_field( $this->query->get_query_var( 'sitemap-stylesheet' ) );
    $paged           = $this->absint( $this->query->get_query_var( 'paged' ) );

   

    if ( ! $this->sitemap->sitemaps_enabled() ) {
        $this->query->set_404();
        $this->status_header( 404 );
        return;
    }

    // Render stylesheet if this is stylesheet route.
    if ( $stylesheet_type ) {
        

        $this->sitemap_style->render_stylesheet( $stylesheet_type );
        exit;
    }

    // Render the index.
    if ( 'index' === $sitemap ) {
        $sitemap_list = $this->index->get_sitemap_list();

        $this->renderer->render_index( $sitemap_list );
        exit;
    }

    $provider = $this->registry->get_provider( $sitemap );

    if ( ! $provider ) {
        return;
    }

    if ( empty( $paged ) ) {
        $paged = 1;
    }

    $url_list = $provider->get_url_list( $paged, $object_subtype );

    
    if ( empty( $url_list ) ) {
        $this->query->set_404();
        $this->status_header( 404 );
        return;
    }

    $this->sitemap_render->render_sitemap( $url_list );
    exit;
}

public function sanitize_text_field( $str ) {
	$filtered = $this->_sanitize_text_fields( $str, false );

	
	return $this->hook->apply_filters( 'sanitize_text_field', $filtered, $str );
}

private function _sanitize_text_fields( $str, $keep_newlines = false ) {
	if ( is_object( $str ) || is_array( $str ) ) {
		return '';
	}

	$str = (string) $str;

	$filtered = $this->format->wp_check_invalid_utf8( $str );

	if ( strpos( $filtered, '<' ) !== false ) {
		$filtered = $this->format->wp_pre_kses_less_than( $filtered );
		// This will strip extra whitespace for us.
		$filtered = $this->format->wp_strip_all_tags( $filtered, false );

		// Use HTML entities in a special case to make sure no later
		// newline stripping stage could lead to a functional tag.
		$filtered = str_replace( "<\n", "&lt;\n", $filtered );
	}

	if ( ! $keep_newlines ) {
		$filtered = preg_replace( '/[\r\n\t ]+/', ' ', $filtered );
	}
	$filtered = trim( $filtered );

	$found = false;
	while ( preg_match( '/%[a-f0-9]{2}/i', $filtered, $match ) ) {
		$filtered = str_replace( $match[0], '', $filtered );
		$found    = true;
	}

	if ( $found ) {
		// Strip out the whitespace that may now exist after removing the octets.
		$filtered = trim( preg_replace( '/ +/', ' ', $filtered ) );
	}

	return $filtered;
}

public function absint(  ) {
	return $this->abs( (int) $this->maybeint );
}

private function abs(): void{
     echo 'el maybeint es: '. $this->maybeint;
}


public function wp_check_invalid_utf8( $string, $strip = false ) {
	$string = (string) $string;

	if ( 0 === strlen( $string ) ) {
		return '';
	}

	// Store the site charset as a static to avoid multiple calls to get_option().
	static $is_utf8 = null;
	if ( ! isset( $is_utf8 ) ) {
		$is_utf8 = in_array( $this->opt->get_option( 'blog_charset' ), array( 'utf8', 'utf-8', 'UTF8', 'UTF-8' ), true );
	}
	if ( ! $is_utf8 ) {
		return $string;
	}

	// Check for support for utf8 in the installed PCRE library once and store the result in a static.
	static $utf8_pcre = null;
	if ( ! isset( $utf8_pcre ) ) {
		// phpcs:ignore WordPress.PHP.NoSilencedErrors.Discouraged
		$utf8_pcre = @preg_match( '/^./u', 'a' );
	}
	// We can't demand utf8 in the PCRE installation, so just return the string in those cases.
	if ( ! $utf8_pcre ) {
		return $string;
	}

	// phpcs:ignore WordPress.PHP.NoSilencedErrors.Discouraged -- preg_match fails when it encounters invalid UTF8 in $string.
	if ( 1 === @preg_match( '/^./us', $string ) ) {
		return $string;
	}

	// Attempt to strip the bad chars if requested (not recommended).
	if ( $strip && function_exists( 'iconv' ) ) {
		return iconv( 'utf-8', 'utf-8', $string );
	}

	return '';
}

public function _deprecated_argument( $function, $version, $message = '' ) {

	/**
	 * Fires when a deprecated argument is called.
	 *
	 * @since 3.0.0
	 *
	 * @param string $function The function that was called.
	 * @param string $message  A message regarding the change.
	 * @param string $version  The version of WordPress that deprecated the argument used.
	 */
	$this->hook->do_action( 'deprecated_argument_run', $function, $message, $version );

	/**
	 * Filters whether to trigger an error for deprecated arguments.
	 *
	 * @since 3.0.0
	 *
	 * @param bool $trigger Whether to trigger the error for deprecated arguments. Default true.
	 */
	if ( POL_DEBUG && $this->hook->apply_filters( 'deprecated_argument_trigger_error', true ) ) {
		if ( function_exists( '__' ) ) {
			if ( $message ) {
				trigger_error(
					sprintf(
						/* translators: 1: PHP function name, 2: Version number, 3: Optional message regarding the change. */
						$this->ident->__( '%1$s was called with an argument that is <strong>deprecated</strong> since version %2$s! %3$s' ),
						$function,
						$version,
						$message
					),
					E_USER_DEPRECATED
				);
			} else {
				trigger_error(
					sprintf(
						/* translators: 1: PHP function name, 2: Version number. */
						$this->ident->__( '%1$s was called with an argument that is <strong>deprecated</strong> since version %2$s with no alternative available.' ),
						$function,
						$version
					),
					E_USER_DEPRECATED
				);
			}
		} else {
			if ( $message ) {
				trigger_error(
					sprintf(
						'%1$s was called with an argument that is <strong>deprecated</strong> since version %2$s! %3$s',
						$function,
						$version,
						$message
					),
					E_USER_DEPRECATED
				);
			} else {
				trigger_error(
					sprintf(
						'%1$s was called with an argument that is <strong>deprecated</strong> since version %2$s with no alternative available.',
						$function,
						$version
					),
					E_USER_DEPRECATED
				);
			}
		}
	}
}

private function status_header( $code, $description = '' ) {
	if ( ! $description ) {
		$description = $this->get_status_header_desc( $code );
	}

	if ( empty( $description ) ) {
		return;
	}

	$protocol      = $this->traducir->wp_get_server_protocol();
	$status_header = "$protocol $code $description";
	if ( function_exists( 'apply_filters' ) ) {

		/**
		 * Filters an HTTP status header.
		 *
		 * @since 2.2.0
		 *
		 * @param string $status_header HTTP status header.
		 * @param int    $code          HTTP status code.
		 * @param string $description   Description for the status code.
		 * @param string $protocol      Server protocol.
		 */
		$status_header = $this->hook->apply_filters( 'status_header', $status_header, $code, $description, $protocol );
	}

	if ( ! headers_sent() ) {
		header( $status_header, true, $code );
	}
}

public function get_main_network_id() {
	if ( ! $this->traducir->is_multisite() ) {
		return 1;
	}

	$current_network = $this->ms_networl->get_network();

	if ( defined( 'PRIMARY_NETWORK_ID' ) ) {
		$main_network_id = PRIMARY_NETWORK_ID;
	} elseif ( isset( $current_network->id ) && 1 === (int) $current_network->id ) {
		// If the current network has an ID of 1, assume it is the main network.
		$main_network_id = 1;
	} else {
		$_networks       = $this->ms_networl->get_networks(
			array(
				'fields' => 'ids',
				'number' => 1,
			)
		);
		$main_network_id = array_shift( $_networks );
	}

	/**
	 * Filters the main network ID.
	 *
	 * @since 4.3.0
	 *
	 * @param int $main_network_id The ID of the main network.
	 */
	return (int) $this->hook->apply_filters( 'get_main_network_id', $main_network_id );
}


public function maybe_unserialize( $data ) {
	if ( $this->is_serialized( $data ) ) { // Don't attempt to unserialize data that wasn't serialized going in.
		return @unserialize( trim( $data ) );
	}

	return $data;
}

private function is_serialized( $data, $strict = true ) {
	// If it isn't a string, it isn't serialized.
	if ( ! is_string( $data ) ) {
		return false;
	}
	$data = trim( $data );
	if ( 'N;' === $data ) {
		return true;
	}
	if ( strlen( $data ) < 4 ) {
		return false;
	}
	if ( ':' !== $data[1] ) {
		return false;
	}
	if ( $strict ) {
		$lastc = substr( $data, -1 );
		if ( ';' !== $lastc && '}' !== $lastc ) {
			return false;
		}
	} else {
		$semicolon = strpos( $data, ';' );
		$brace     = strpos( $data, '}' );
		// Either ; or } must exist.
		if ( false === $semicolon && false === $brace ) {
			return false;
		}
		// But neither must be in the first X characters.
		if ( false !== $semicolon && $semicolon < 3 ) {
			return false;
		}
		if ( false !== $brace && $brace < 4 ) {
			return false;
		}
	}
	$token = $data[0];
	switch ( $token ) {
		case 's':
			if ( $strict ) {
				if ( '"' !== substr( $data, -2, 1 ) ) {
					return false;
				}
			} elseif ( false === strpos( $data, '"' ) ) {
				return false;
			}
			// Or else fall through.
		case 'a':
		case 'O':
			return (bool) preg_match( "/^{$token}:[0-9]+:/s", $data );
		case 'b':
		case 'i':
		case 'd':
			$end = $strict ? '$' : '';
			return (bool) preg_match( "/^{$token}:[0-9.E+-]+;$end/", $data );
	}
	return false;
}

private function get_status_header_desc( $code ) {
	global $wp_header_to_desc;

	$code = $this->absint( $code );

	if ( ! isset( $wp_header_to_desc ) ) {
		$wp_header_to_desc = array(
			100 => 'Continue',
			101 => 'Switching Protocols',
			102 => 'Processing',
			103 => 'Early Hints',

			200 => 'OK',
			201 => 'Created',
			202 => 'Accepted',
			203 => 'Non-Authoritative Information',
			204 => 'No Content',
			205 => 'Reset Content',
			206 => 'Partial Content',
			207 => 'Multi-Status',
			226 => 'IM Used',

			300 => 'Multiple Choices',
			301 => 'Moved Permanently',
			302 => 'Found',
			303 => 'See Other',
			304 => 'Not Modified',
			305 => 'Use Proxy',
			306 => 'Reserved',
			307 => 'Temporary Redirect',
			308 => 'Permanent Redirect',

			400 => 'Bad Request',
			401 => 'Unauthorized',
			402 => 'Payment Required',
			403 => 'Forbidden',
			404 => 'Not Found',
			405 => 'Method Not Allowed',
			406 => 'Not Acceptable',
			407 => 'Proxy Authentication Required',
			408 => 'Request Timeout',
			409 => 'Conflict',
			410 => 'Gone',
			411 => 'Length Required',
			412 => 'Precondition Failed',
			413 => 'Request Entity Too Large',
			414 => 'Request-URI Too Long',
			415 => 'Unsupported Media Type',
			416 => 'Requested Range Not Satisfiable',
			417 => 'Expectation Failed',
			418 => 'I\'m a teapot',
			421 => 'Misdirected Request',
			422 => 'Unprocessable Entity',
			423 => 'Locked',
			424 => 'Failed Dependency',
			426 => 'Upgrade Required',
			428 => 'Precondition Required',
			429 => 'Too Many Requests',
			431 => 'Request Header Fields Too Large',
			451 => 'Unavailable For Legal Reasons',

			500 => 'Internal Server Error',
			501 => 'Not Implemented',
			502 => 'Bad Gateway',
			503 => 'Service Unavailable',
			504 => 'Gateway Timeout',
			505 => 'HTTP Version Not Supported',
			506 => 'Variant Also Negotiates',
			507 => 'Insufficient Storage',
			510 => 'Not Extended',
			511 => 'Network Authentication Required',
		);
	}

	if ( isset( $wp_header_to_desc[ $code ] ) ) {
		return $wp_header_to_desc[ $code ];
	} else {
		return '';
	}
}



}
?>