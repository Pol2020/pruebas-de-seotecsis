<?php
namespace cms\pol\elaborado;

require __DIR__ . '/pol-theme.php';
require __DIR__ . '/functions.php';
require __DIR__ . '/pol-ms-network.php';

use cms\pol\elaborado\Theme as Theme;
use cms\pol\elaborado\Functions as Functions;
use cms\pol\elaborado\Ms_Network as Ms_Network;


class Traducciones extends Hook{

    protected $theme;
    protected $func;
    protected $ms_network;

    public function __construct()
    {
        $this->theme = new Theme();
        $this->func = new Functions();
        $this->ms_network = new Ms_Network();
    }

   public function wp_load_translations_early() {
        global $wp_locale;
    
        static $loaded = false;

        if ( $loaded ) {
            return;
        }
        $loaded = true;
    
      /*  if ( function_exists( 'did_action' ) && did_action( 'init' ) ) {
            return;
        }
    */
        // We need $wp_local_package.
       // require ABSPATH . WPINC . '/version.php';
    
        // Translation and localization.
      //  require_once ABSPATH . WPINC . '/pomo/mo.php';
      //  require_once ABSPATH . WPINC . '/l10n.php';
        require_once ABSPATH . POLINC . '/class-pol-locale.php';
        require_once ABSPATH . POLINC . '/class-pol-locale-switcher.php';
    
        // General libraries.
        require_once ABSPATH . POLINC . '/pol-plugin.php';
    
        $locales   = array();
        $locations = array();
    
        while ( true ) {
            if ( defined( 'WPLANG' ) ) {
                if ( '' === WPLANG ) {
                    break;
                }
                $locales[] = WPLANG;
            }
    
          /*  if ( isset( $wp_local_package ) ) {
                $locales[] = $wp_local_package;
            }*/
    
            if ( ! $locales ) {
                break;
            }
    
            if ( defined( 'WP_LANG_DIR' ) && @is_dir( WP_LANG_DIR ) ) {
                $locations[] = WP_LANG_DIR;
            }
    
            if ( defined( 'WP_CONTENT_DIR' ) && @is_dir( POL_CONTENT_DIR . '/languages' ) ) {
                $locations[] = POL_CONTENT_DIR . '/languages';
            }
    
            if ( @is_dir( ABSPATH . 'codigo/languages' ) ) {
                $locations[] = ABSPATH . 'codigo/languages';
            }
    
            if ( @is_dir( ABSPATH . POLINC . '/languages' ) ) {
                $locations[] = ABSPATH . POLINC . '/languages';
            }
    
            if ( ! $locations ) {
                break;
            }
    
            $locations = array_unique( $locations );
    
            foreach ( $locales as $locale ) {
                foreach ( $locations as $location ) {
                    if ( file_exists( $location . '/' . $locale . '.mo' ) ) {
                        $this->theme->load_textdomain( 'default', $location . '/' . $locale . '.mo' );
                        if ( defined( 'WP_SETUP_CONFIG' ) && file_exists( $location . '/admin-' . $locale . '.mo' ) ) {
                            $this->theme->load_textdomain( 'default', $location . '/admin-' . $locale . '.mo' );
                        }
                        break 2;
                    }
                }
            }
    
            break;
        }
    
    }

    public function wp_installing( $is_installing = null ) {
        static $installing = null;
    
        if ( is_null( $installing ) ) {
            $installing = defined( 'WP_INSTALLING' ) && WP_INSTALLING;
        }
    
        if ( ! is_null( $is_installing ) ) {
            $old_installing = $installing;
            $installing     = $is_installing;
            return (bool) $old_installing;
        }
    
        return (bool) $installing;
    }

    public function is_multisite() {
        if ( defined( 'MULTISITE' ) ) {
            return MULTISITE;
        }
    
        if ( defined( 'SUBDOMAIN_INSTALL' ) || defined( 'VHOST' ) || defined( 'SUNRISE' ) ) {
            return true;
        }
    
        return false;
    }
  
    public function get_current_network_id() {
        if ( ! $this->is_multisite() ) {
            return 1;
        }
    
        $current_network = $this->ms_network->get_network();
    
        if ( ! isset( $current_network->id ) ) {
            return $this->func->get_main_network_id();
        }
    
        return $this->func->absint( $current_network->id );
    }

    public function wp_get_server_protocol() {
        $protocol = isset( $_SERVER['SERVER_PROTOCOL'] ) ? $_SERVER['SERVER_PROTOCOL'] : '';
        if ( ! in_array( $protocol, array( 'HTTP/1.1', 'HTTP/2', 'HTTP/2.0' ), true ) ) {
            $protocol = 'HTTP/1.0';
        }
        return $protocol;
    }


}
?>