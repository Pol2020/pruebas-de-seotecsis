<?php
namespace cms\pol\elaborado;

require_once __DIR__ . '/pol-hook.php';
require_once __DIR__ . '/pol-load.php';
require_once __DIR__ . '/functions.php';
require_once __DIR__ . '/pol-cache.php';
require_once __DIR__ . '/pol-plugin.php';
require __DIR__ . '/pol-I10n.php';
require __DIR__ . '/pol-formatting.php';

use cms\pol\elaborado\Hook as Hook;
use cms\pol\elaborado\Traducciones aS Traducciones;
use cms\pol\elaborado\Functions as Functions;
use cms\pol\elaborado\Cache aS Cache;
use cms\pol\elaborado\Identacion as Identacion;
use cms\pol\elaborado\Formato as Formato;

      

class Opciones{

 protected $hook;
 protected $traducion;
 protected $ident;
 protected $format;
 protected $cache;
 protected $func;
       
     public function __construct(){

        
        $this->hook = new Hook();
             
        $this->traducion = new Traducciones();
        
        $this->cache = new Cache(); 

        $this->ident = new Identacion();

        $this->format = new Formato();

        $this->func = new Functions();
          
         $this->option = "habia una vez en el archivo nucleo/pol-option.php";
    
  
    }

    public function get_option() {
      
       
      
        $this->option = trim($this->option );

        if ( empty( $$this->option ) ) {
            return false;
        }
    
       
        $deprecated_keys = array(
            'blacklist_keys'    => 'disallowed_keys',
            'comment_whitelist' => 'comment_previously_approved',
        );
    
        if ( ! $traduccion->wp_installing() && isset( $deprecated_keys[ $this->option] ) ) {
            $this->function->_deprecated_argument(
                __FUNCTION__,
                '5.5.0',
                sprintf(
                    /* translators: 1: Deprecated $this->option key, 2: New $this->option key. */
                    $this->ident->__( 'The "%1$s" $this->option key has been renamed to "%2$s".' ),
                    $this->option,
                    $deprecated_keys[ $this->option ]
                )
            );
            return $this->get_option( $deprecated_keys[ $this->option ] );
        }
    
      
        $pre = $this->hook->apply_filters( "pre_option_{$this->option}", false, $this->$this->option );
    
        if ( false !== $pre ) {
            return $pre;
        }
    
        if ( defined( 'WP_SETUP_CONFIG' ) ) {
            return false;
        }
    
        // Distinguish between `false` as a default, and not passing one.
        $passed_default = func_num_args() > 1;
    
        if ( ! $traduccion->wp_installing() ) {
            // Prevent non-existent options from triggering multiple queries.
            $notoptions = $cache->wp_cache_get( 'notoptions', 'options' );
    
            if ( isset( $notoptions[ $this->option ] ) ) {
            
                return $this->hook->apply_filters( "default_option_{$this->option}", $this->option, $passed_default );
            }
    
            $alloptions = $this->wp_load_alloptions();
    
            if ( isset( $alloptions[ $this->option ] ) ) {
                $value = $alloptions[ $this->option ];
            } else {
                $value = $cache->wp_cache_get( $this->option, 'options' );
    
                if ( false === $value ) {
                    $row = $wpdb->get_row( $wpdb->prepare( "SELECT option_value FROM $wpdb->options WHERE option_name = %s LIMIT 1", $this->option ) );
    
                    // Has to be get_row() instead of get_var() because of funkiness with 0, false, null values.
                    if ( is_object( $row ) ) {
                        $value = $row->option_value;
                        $cache->wp_cache_add( $this->option, $value, 'options' );
                    } else { // Option does not exist, so we must cache its non-existence.
                        if ( ! is_array( $notoptions ) ) {
                            $notoptions = array();
                        }
    
                        $notoptions[ $this->option ] = true;
                        $cache->wp_cache_set( 'notoptions', $notoptions, 'options' );
    
                        /** This filter is documented in nucleo/$this->option.php */
                        return $hook->apply_filters( "default_option_{$this->option}", $default, $this->option, $passed_default );
                    }
                }
            }
        } else {
            $suppress = $wpdb->suppress_errors();
            $row      = $wpdb->get_row( $wpdb->prepare( "SELECT option_value FROM $wpdb->options WHERE option_name = %s LIMIT 1", $this->option ) );
            $wpdb->suppress_errors( $suppress );
    
            if ( is_object( $row ) ) {
                $value = $row->option_value;
            } else {
                /** This filter is documented in nucleo/$this->option.php */
                return $this->hook->apply_filters( "default_option_{$this->option}", $this->option, $passed_default );
            }
        }
    
        // If home is not set, use siteurl.
        if ( 'home' === $this->option && '' === $value ) {
            return $this->get_option( 'siteurl' );
        }
    
        if ( in_array( $this->option, array( 'siteurl', 'home', 'category_base', 'tag_base' ), true ) ) {
            $value = $this->format->untrailingslashit( $value );
        }
    
        /**
         * Filters the value of an existing $this->option.
         *
         * The dynamic portion of the hook name, `$$this->option`, refers to the $this->option name.
         *
         * @since 1.5.0 As 'option_' . $setting
         * @since 3.0.0
         * @since 4.4.0 The `$$this->option` parameter was added.
         *
         * @param mixed  $value  Value of the $this->option. If stored serialized, it will be
         *                       unserialized prior to being returned.
         * @param string $$this->option Option name.
         */
        return $this->hook->apply_filters( "option_{$this->option}", $this->func ->maybe_unserialize( $value ), $this->option );
    }

    private function wp_load_alloptions( $force_cache = false ) {
        global $wpdb;
    
        if ( ! $this->traduccion->wp_installing() || ! $this->traducion->is_multisite() ) {
            $alloptions = $this->cache->wp_cache_get( 'alloptions', 'options', $force_cache );
        } else {
            $alloptions = false;
        }
    
        if ( ! $alloptions ) {
            $suppress      = $wpdb->suppress_errors();
            $alloptions_db = $wpdb->get_results( "SELECT option_name, option_value FROM $wpdb->options WHERE autoload = 'yes'" );
            if ( ! $alloptions_db ) {
                $alloptions_db = $wpdb->get_results( "SELECT option_name, option_value FROM $wpdb->options" );
            }
            $wpdb->suppress_errors( $suppress );
    
            $alloptions = array();
            foreach ( (array) $alloptions_db as $o ) {
                $alloptions[ $o->option_name ] = $o->option_value;
            }
    
            if ( ! $this->traduccion->wp_installing() || ! $this->traducion->is_multisite() ) {
                /**
                 * Filters all options before caching them.
                 *
                 * @since 4.9.0
                 *
                 * @param array $alloptions Array with all options.
                 */
                $alloptions = $this->hook->apply_filters( 'pre_cache_alloptions', $alloptions );
    
                $this->cache->wp_cache_add( 'alloptions', $alloptions, 'options' );
            }
        }
    
        /**
         * Filters all options after retrieving them.
         *
         * @since 4.9.0
         *
         * @param array $alloptions Array with all options.
         */
        return $this->hook->apply_filters( 'alloptions', $alloptions );
    }

    public function get_site_option( $option, $default = false, $deprecated = true ) {
        return $this->get_network_option( null, $option, $default );
    }

    private function get_network_option( $network_id, $option, $default = false ) {
        global $wpdb;
      
    
        if ( $network_id && ! is_numeric( $network_id ) ) {
            return false;
        }
    
        $network_id = (int) $network_id;
    
        // Fallback to the current network if a network ID is not specified.
        if ( ! $network_id ) {
            $network_id = $this->traducion->get_current_network_id();
        }
    
        /**
         * Filters the value of an existing network option before it is retrieved.
         *
         * The dynamic portion of the hook name, `$option`, refers to the option name.
         *
         * Returning a truthy value from the filter will effectively short-circuit retrieval
         * and return the passed value instead.
         *
         * @since 2.9.0 As 'pre_site_option_' . $key
         * @since 3.0.0
         * @since 4.4.0 The `$option` parameter was added.
         * @since 4.7.0 The `$network_id` parameter was added.
         * @since 4.9.0 The `$default` parameter was added.
         *
         * @param mixed  $pre_option The value to return instead of the option value. This differs
         *                           from `$default`, which is used as the fallback value in the event
         *                           the option doesn't exist elsewhere in get_network_option().
         *                           Default false (to skip past the short-circuit).
         * @param string $option     Option name.
         * @param int    $network_id ID of the network.
         * @param mixed  $default    The fallback value to return if the option does not exist.
         *                           Default false.
         */
        $pre = $this->hook->apply_filters( "pre_site_option_{$option}", false, $option, $network_id, $default );
    
        if ( false !== $pre ) {
            return $pre;
        }
    
        // Prevent non-existent options from triggering multiple queries.
        $notoptions_key = "$network_id:notoptions";
        $notoptions     = $this->cache->wp_cache_get( $notoptions_key, 'site-options' );
    
        if ( is_array( $notoptions ) && isset( $notoptions[ $option ] ) ) {
    
            /**
             * Filters a specific default network option.
             *
             * The dynamic portion of the hook name, `$option`, refers to the option name.
             *
             * @since 3.4.0
             * @since 4.4.0 The `$option` parameter was added.
             * @since 4.7.0 The `$network_id` parameter was added.
             *
             * @param mixed  $default    The value to return if the site option does not exist
             *                           in the database.
             * @param string $option     Option name.
             * @param int    $network_id ID of the network.
             */
            return $this->hook->apply_filters( "default_site_option_{$option}", $default, $option, $network_id );
        }
    
        if ( ! $this->traducion->is_multisite() ) {
            /** This filter is documented in nucleo/option.php */
            $default = $this->hook->apply_filters( 'default_site_option_' . $option, $default, $option, $network_id );
            $value   = $this->get_option( $option, $default );
        } else {
            $cache_key = "$network_id:$option";
            $value     = $this->cache->wp_cache_get( $cache_key, 'site-options' );
    
            if ( ! isset( $value ) || false === $value ) {
                $row = $wpdb->get_row( $wpdb->prepare( "SELECT meta_value FROM $wpdb->sitemeta WHERE meta_key = %s AND site_id = %d", $option, $network_id ) );
    
                // Has to be get_row() instead of get_var() because of funkiness with 0, false, null values.
                if ( is_object( $row ) ) {
                    $value = $row->meta_value;
                    $value = $this->func->maybe_unserialize( $value );

                    $this->cache->wp_cache_set( $cache_key, $value, 'site-options' );
                } else {
                    if ( ! is_array( $notoptions ) ) {
                        $notoptions = array();
                    }
    
                    $notoptions[ $option ] = true;
                    $this->cache->wp_cache_set( $notoptions_key, $notoptions, 'site-options' );
    
                    /** This filter is documented in nucleo/option.php */
                    $value = $this->hook->apply_filters( 'default_site_option_' . $option, $default, $option, $network_id );
                }
            }
        }
    
        if ( ! is_array( $notoptions ) ) {
            $notoptions = array();
            $this->cache->wp_cache_set( $notoptions_key, $notoptions, 'site-options' );
        }
    
        /**
         * Filters the value of an existing network option.
         *
         * The dynamic portion of the hook name, `$option`, refers to the option name.
         *
         * @since 2.9.0 As 'site_option_' . $key
         * @since 3.0.0
         * @since 4.4.0 The `$option` parameter was added.
         * @since 4.7.0 The `$network_id` parameter was added.
         *
         * @param mixed  $value      Value of network option.
         * @param string $option     Option name.
         * @param int    $network_id ID of the network.
         */
        return $this->hook->apply_filters( "site_option_{$option}", $value, $option, $network_id );
    }



}