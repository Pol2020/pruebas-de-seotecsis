<?php
namespace cms\pol\elaborado;

require __DIR__ . '/pol-plugin.php';
use cms\pol\elaborado\Hook as Hook;

class Theme{

    protected $hook;

    public function __construct()
    {
    
      $this->hook = new Hook();
        
    }

    public function load_textdomain( $domain, $mofile ) {
        // global $l10n, $l10n_unloaded;
     
       //  $l10n_unloaded = (array) $l10n_unloaded;
     
         /**
          * Filters whether to override the .mo file loading.
          *
          * @since 2.9.0
          *
          * @param bool   $override Whether to override the .mo file loading. Default false.
          * @param string $domain   Text domain. Unique identifier for retrieving translated strings.
          * @param string $mofile   Path to the MO file.
          */
         $plugin_override = $this->hook->apply_filters( 'override_load_textdomain', false, $domain, $mofile );
     /*
         if ( true === (bool) $plugin_override ) {
             unset( $l10n_unloaded[ $domain ] );
     
             return true;
         }
     */
         /**
          * Fires before the MO translation file is loaded.
          *
          * @since 2.9.0
          *
          * @param string $domain Text domain. Unique identifier for retrieving translated strings.
          * @param string $mofile Path to the .mo file.
          */
          $this->hook->do_action( 'load_textdomain', $domain, $mofile );
     
         /**
          * Filters MO file path for loading translations for a specific text domain.
          *
          * @since 2.9.0
          *
          * @param string $mofile Path to the MO file.
          * @param string $domain Text domain. Unique identifier for retrieving translated strings.
          */
         $mofile = $this->hook->apply_filters( 'load_textdomain_mofile', $mofile, $domain );
     /*
         if ( ! is_readable( $mofile ) ) {
             return false;
         }
     
         $mo = new MO();
         if ( ! $mo->import_from_file( $mofile ) ) {
             return false;
         }
     
         if ( isset( $l10n[ $domain ] ) ) {
             $mo->merge_with( $l10n[ $domain ] );
         }
     */
       // unset( $l10n_unloaded[ $domain ] );
     
       //  $l10n[ $domain ] = &$mo;
     
         return true;
     }

}