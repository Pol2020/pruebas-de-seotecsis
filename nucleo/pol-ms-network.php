<?php
namespace cms\pol\elaborado;

require __DIR__ . '/pol-plugin.php';

use cms\pol\elaborado\Hook as Hook;

class Ms_Network{

    protected $hook;

    public function __construct()
    {
        $this->hook = new Hook();
    }

    public function get_network( $network = null ) {
        global $current_site;
        if ( empty( $network ) && isset( $current_site ) ) {
            $network = $current_site;
        }
    
        if ( $network instanceof Network ) {
            $_network = $network;
        } elseif ( is_object( $network ) ) {
            $_network = new Network( $network );
        } else {
            $_network = Network::get_instance( $network );
        }
    
        if ( ! $_network ) {
            return null;
        }
    
       
        $_network = $this->hook->apply_filters( 'get_network', $_network );
    
        return $_network;
    }


    function get_networks( $args = array() ) {
        $query = new Network_Query();
    
        return $query->query( $args );
    }


}