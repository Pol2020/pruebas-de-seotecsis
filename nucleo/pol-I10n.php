<?php
namespace cms\pol\elaborado;

require_once __DIR__ . '/pol-plugin.php';
require __DIR__ . '/pol-theme.php';
require __DIR__ . '/pol-option.php';
require __DIR__ . '/pol-load.php';

use cms\pol\elaborado\Hook as Hook;
use cms\pol\elaborado\Theme as Theme;
use cms\pol\elaborado\Opciones as Opciones;
use cms\pol\elaborado\Traducciones as Traducciones;

class Identacion{

    protected $hook;
    protected $theme;
    protected $opt;
    protected $traducir;

    public function __construct()
    {
        
        $this->hook =new Hook();
      
        $this->theme = new Theme();
       
        $this->opt = new Opciones();

        $this->traducir = new Traducciones();
    }

    public function __( $text, $domain = 'default' ) {
        return $this->translate( $text, $domain );
    }
    
    private function translate( $text, $domain = 'default' ) {
        $translations = $this->get_translations_for_domain( $domain );
        $translation  = $translations->translate( $text );
    
      
        $translation = $this->hook->apply_filters( 'gettext', $translation, $text, $domain );
    
       
        $translation = $this->hook->apply_filters( "gettext_{$domain}", $translation, $text, $domain );
    
        return $translation;
    }

    private function get_translations_for_domain( $domain ) {
        global $l10n;
        if ( isset( $l10n[ $domain ] ) || ( $this->_load_textdomain_just_in_time( $domain ) && isset( $l10n[ $domain ] ) ) ) {
            return $l10n[ $domain ];
        }
    
        static $noop_translations = null;
        if ( null === $noop_translations ) {
            $noop_translations = new NOOP_Translations;
        }
    
        return $noop_translations;
    }
    
    private function _load_textdomain_just_in_time( $domain ) {
        global $l10n_unloaded;
    
        $l10n_unloaded = (array) $l10n_unloaded;
    
        // Short-circuit if domain is 'default' which is reserved for core.
        if ( 'default' === $domain || isset( $l10n_unloaded[ $domain ] ) ) {
            return false;
        }
    
        $translation_path = $this->_get_path_to_translation( $domain );
        if ( false === $translation_path ) {
            return false;
        }
    
        return $this->theme->load_textdomain( $domain, $translation_path );
    }

    private function _get_path_to_translation( $domain, $reset = false ) {
        static $available_translations = array();
    
        if ( true === $reset ) {
            $available_translations = array();
        }
    
        if ( ! isset( $available_translations[ $domain ] ) ) {
            $available_translations[ $domain ] = $this->_get_path_to_translation_from_lang_dir( $domain );
        }
    
        return $available_translations[ $domain ];
    }
    
    private  function _get_path_to_translation_from_lang_dir( $domain ) {
        static $cached_mofiles = null;
    
        if ( null === $cached_mofiles ) {
            $cached_mofiles = array();
    
            $locations = array(
                POL_LANG_DIR . '/plugins',
                POL_LANG_DIR . '/themes',
            );
    
            foreach ( $locations as $location ) {
                $mofiles = glob( $location . '/*.mo' );
                if ( $mofiles ) {
                    $cached_mofiles = array_merge( $cached_mofiles, $mofiles );
                }
            }
        }
    
        $locale = $this->determine_locale();
        $mofile = "{$domain}-{$locale}.mo";
    
        $path = POL_LANG_DIR . '/plugins/' . $mofile;
        if ( in_array( $path, $cached_mofiles, true ) ) {
            return $path;
        }
    
        $path = POL_LANG_DIR . '/themes/' . $mofile;
        if ( in_array( $path, $cached_mofiles, true ) ) {
            return $path;
        }
    
        return false;
    }

    private function determine_locale() {
	
        /**
         * Filters the locale for the current request prior to the default determination process.
         *
         * Using this filter allows to override the default logic, effectively short-circuiting the function.
         *
         * @since 5.0.0
         *
         * @param string|null $locale The locale to return and short-circuit. Default null.
         */
        $determined_locale = $this->hook->apply_filters( 'pre_determine_locale', null );
    
        if ( ! empty( $determined_locale ) && is_string( $determined_locale ) ) {
            return $determined_locale;
        }
    
        $determined_locale = $this->get_locale();
    /*
        if ( is_admin() ) {
            $determined_locale = get_user_locale();
        }
    
        if ( isset( $_GET['_locale'] ) && 'user' === $_GET['_locale'] && wp_is_json_request() ) {
            $determined_locale = get_user_locale();
        }
    
        if ( ! empty( $_GET['wp_lang'] ) && ! empty( $GLOBALS['pagenow'] ) && 'wp-login.php' === $GLOBALS['pagenow'] ) {
            $determined_locale = sanitize_text_field( $_GET['wp_lang'] );
        }
    */
    echo 'funcion determine_locale';
        /**
         * Filters the locale for the current request.
         *
         * @since 5.0.0
         *
         * @param string $locale The locale.
         */
        return $this->hook->apply_filters( 'determine_locale', $determined_locale );
    }

    private function get_locale() {
        global $locale, $wp_local_package;
    
        $hijos_funciones = new Funciones();
    
        if ( isset( $locale ) ) {
            /**
             * Filters the locale ID of the WordPress installation.
             *
             * @since 1.5.0
             *
             * @param string $locale The locale ID.
             */
            return $this->hook->apply_filters( 'locale', $locale );
        }
    
        if ( isset( $wp_local_package ) ) {
            $locale = $wp_local_package;
        }
    
        // WPLANG was defined in wp-config.
        if ( defined( 'WPLANG' ) ) {
            $locale = WPLANG;
        }
    
        // If multisite, check options.
        if ( $this->traducir->is_multisite() ) {
            // Don't check blog option when installing.
            if ( $this->traducir->wp_installing() ) {
                $ms_locale = $this->opt->get_site_option( 'WPLANG' );
            } else {
                $ms_locale = $this->opt->get_option( 'WPLANG' );
                if ( false === $ms_locale ) {
                    $ms_locale = $this->opt->get_site_option( 'WPLANG' );
                }
            }
    
            if ( false !== $ms_locale ) {
                $locale = $ms_locale;
            }
        } else {
            $db_locale = $hijos_funciones->get_option( 'WPLANG' );
            if ( false !== $db_locale ) {
                $locale = $db_locale;
            }
        }
    
        if ( empty( $locale ) ) {
            $locale = 'en_US';
        }
    
        /** This filter is documented in nucleo/l10n.php */
        return $this->hook->apply_filters( 'locale', $locale );
    }

    private  function translate_with_gettext_context( $text, $context, $domain = 'default' ) {
        $translations = $this->get_translations_for_domain( $domain );
        $translation  = $translations->translate( $text, $context );
    
        /**
         * Filters text with its translation based on context information.
         *
         * @since 2.8.0
         *
         * @param string $translation Translated text.
         * @param string $text        Text to translate.
         * @param string $context     Context information for the translators.
         * @param string $domain      Text domain. Unique identifier for retrieving translated strings.
         */
        $translation = $this->hook->apply_filters( 'gettext_with_context', $translation, $text, $context, $domain );
    
        /**
         * Filters text with its translation based on context information for a domain.
         *
         * The dynamic portion of the hook, `$domain`, refers to the text domain.
         *
         * @since 5.5.0
         *
         * @param string $translation Translated text.
         * @param string $text        Text to translate.
         * @param string $context     Context information for the translators.
         * @param string $domain      Text domain. Unique identifier for retrieving translated strings.
         */
        $translation = $this->hook->apply_filters( "gettext_with_context_{$domain}", $translation, $text, $context, $domain );
    
        return $translation;
    }

    public function _x( $text, $context, $domain = 'default' ) {
        return $this->translate_with_gettext_context( $text, $context, $domain );
    }

}