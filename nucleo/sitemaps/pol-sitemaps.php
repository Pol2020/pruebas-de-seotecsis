<?php
namespace cms\pol\elaborado;

require_once __DIR__ . '/pol-hook.php';
require_once __DIR__ . '/pol-option.php';

use cms\pol\elaborado\Hook as Hook;
use cms\pol\elaborado\Opciones as Opciones;

class SiteMaps{

    protected $opt;
    protected $hook;

    public function __construct()
    {
        $this->hook = new Hook();

        
        $this->opt = new Opciones();
    }

    public function sitemaps_enabled() {
		$is_enabled = (bool) $this->opt->get_option( 'blog_public' );

		
		return (bool) $this->hook->apply_filters( 'wp_sitemaps_enabled', $is_enabled );
	}

}