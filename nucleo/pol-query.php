<?php
namespace cms\pol\elaborado;

require_once __DIR__ .'pol-plugin.php';

use cms\pol\elaborado\Hook as Hook;

class Query{

    protected $hook;

    public function __construct()
    {
        $this->var = "habia una vez en el archivo nucleo/pol-option.php";
        $this->default = "";
        
        $this->get_query_var();

        
		$this->hook = new Hook();

    }
    
    public function get_query_var() {
           
            return $this->get( $this->key, $this->group, $this->force, $this->found );
        }
    
        private function get(): void{
               echo 'la var es'. $this->var. "<br />"; 
               echo 'el default es'. $this->default. "<br />"; 
               
        }

    public function set_404() {
		$is_feed = $this->is_feed;

		$this->init_query_flags();
		$this->is_404 = true;

		$this->is_feed = $is_feed;

		/**
		 * Fires after a 404 is triggered.
		 *
		 * @since 5.5.0
		 *
		 * @param WP_Query $this The WP_Query instance (passed by reference).
		 */
		$this->hook->do_action_ref_array( 'set_404', array( $this ) );
	}  
    
    private function init_query_flags() {
		$this->is_single            = false;
		$this->is_preview           = false;
		$this->is_page              = false;
		$this->is_archive           = false;
		$this->is_date              = false;
		$this->is_year              = false;
		$this->is_month             = false;
		$this->is_day               = false;
		$this->is_time              = false;
		$this->is_author            = false;
		$this->is_category          = false;
		$this->is_tag               = false;
		$this->is_tax               = false;
		$this->is_search            = false;
		$this->is_feed              = false;
		$this->is_comment_feed      = false;
		$this->is_trackback         = false;
		$this->is_home              = false;
		$this->is_privacy_policy    = false;
		$this->is_404               = false;
		$this->is_paged             = false;
		$this->is_admin             = false;
		$this->is_attachment        = false;
		$this->is_singular          = false;
		$this->is_robots            = false;
		$this->is_favicon           = false;
		$this->is_posts_page        = false;
		$this->is_post_type_archive = false;
	}


}