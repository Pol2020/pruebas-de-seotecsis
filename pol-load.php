<?php
namespace cms\pol\elaborado;


use cms\pol\elaborado\Funciones as Funciones;


error_reporting( E_CORE_ERROR | E_CORE_WARNING | E_COMPILE_ERROR |
 E_ERROR | E_WARNING | E_PARSE | E_USER_ERROR | E_USER_WARNING | 
 E_RECOVERABLE_ERROR );

class Load  {
 
   public function funciones(){
 
    echo 'esto es una prueba';
    return Funciones::class;
   }
    
    
    public function config(){
        if ( file_exists( ABSPATH . 'pol-config.php' ) ) {

            /** The config file resides in ABSPATH */
            require_once ABSPATH . 'pol-config.php';
        
        } elseif ( @file_exists( dirname( ABSPATH ) . '/wp-config.php' ) && ! @file_exists( dirname( ABSPATH ) . '/wp-settings.php' ) ) {
        
            /** The config file resides one level above ABSPATH but is not part of another installation */
            require_once dirname( ABSPATH ) . '/wp-config.php';
        
        } else {
        
            // A config file doesn't exist.
             //  require_once ABSPATH . POLINC . '/pol-load.php';
        
            // Standardize $_SERVER variables across setups.
            $this->wp_fix_server_vars();
        
           // require_once ABSPATH . POLINC . '/pol-functions.php';
        $this->funciones();

            $path = $this->wp_guess_url() . '/gestion/pol-setup-config.php';
            
        
            /*
             * We're going to redirect to setup-config.php. While this shouldn't result
             * in an infinite loop, that's a silly thing to assume, don't you think? If
             * we're traveling in circles, our last-ditch effort is "Need more help?"
             */
            if ( false === strpos( $_SERVER['REQUEST_URI'], 'pol-setup-config' ) ) {
                header( 'Location: ' . $path );
                exit;
            }
        
          
        //    require_once ABSPATH . POLINC . '/version.php';
        
         //   wp_check_php_mysql_versions();
         $traduccion = new Traducciones();
            $traduccion->wp_load_translations_early();
        
             // Die with an error message
            $die = sprintf(
                /* translators: %s: wp-config.php */
                $this->__( "There doesn't seem to be a %s file. I need this before we can get started." ),
                '<code>wp-config.php</code>'
            ) . '</p>';
            $die .= '<p>' . sprintf(
                /* translators: %s: Documentation URL. */
                $this->__( "Need more help? <a href='%s'>We got it</a>." ),
                $this->__( 'https://wordpress.org/support/article/editing-wp-config-php/' )
            ) . '</p>';
            $die .= '<p>' . sprintf(
                /* translators: %s: wp-config.php */
                $locl->__( "You can create a %s file through a web interface, but this doesn't work for all server setups. The safest way is to manually create the file." ),
                '<code>wp-config.php</code>'
            ) . '</p>';
            $die .= '<p><a href="' . $path . '" class="button button-large">' . __( 'Create a Configuration File' ) . '</a>';
        
            $this->wp_die( $die, $this->__( 'WordPress &rsaquo; Error' ) );
        }
    }

    private function wp_fix_server_vars() {
        global $PHP_SELF;
    
        $default_server_values = array(
            'SERVER_SOFTWARE' => '',
            'REQUEST_URI'     => '',
        );
    
        $_SERVER = array_merge( $default_server_values, $_SERVER );
    
        // Fix for IIS when running with PHP ISAPI.
        if ( empty( $_SERVER['REQUEST_URI'] ) || ( 'cgi-fcgi' !== PHP_SAPI && preg_match( '/^Microsoft-IIS\//', $_SERVER['SERVER_SOFTWARE'] ) ) ) {
    
            if ( isset( $_SERVER['HTTP_X_ORIGINAL_URL'] ) ) {
                // IIS Mod-Rewrite.
                $_SERVER['REQUEST_URI'] = $_SERVER['HTTP_X_ORIGINAL_URL'];
            } elseif ( isset( $_SERVER['HTTP_X_REWRITE_URL'] ) ) {
                // IIS Isapi_Rewrite.
                $_SERVER['REQUEST_URI'] = $_SERVER['HTTP_X_REWRITE_URL'];
            } else {
                // Use ORIG_PATH_INFO if there is no PATH_INFO.
                if ( ! isset( $_SERVER['PATH_INFO'] ) && isset( $_SERVER['ORIG_PATH_INFO'] ) ) {
                    $_SERVER['PATH_INFO'] = $_SERVER['ORIG_PATH_INFO'];
                }
    
                // Some IIS + PHP configurations put the script-name in the path-info (no need to append it twice).
                if ( isset( $_SERVER['PATH_INFO'] ) ) {
                    if ( $_SERVER['PATH_INFO'] == $_SERVER['SCRIPT_NAME'] ) {
                        $_SERVER['REQUEST_URI'] = $_SERVER['PATH_INFO'];
                    } else {
                        $_SERVER['REQUEST_URI'] = $_SERVER['SCRIPT_NAME'] . $_SERVER['PATH_INFO'];
                    }
                }
    
                // Append the query string if it exists and isn't null.
                if ( ! empty( $_SERVER['QUERY_STRING'] ) ) {
                    $_SERVER['REQUEST_URI'] .= '?' . $_SERVER['QUERY_STRING'];
                }
            }
        }
    
        // Fix for PHP as CGI hosts that set SCRIPT_FILENAME to something ending in php.cgi for all requests.
        if ( isset( $_SERVER['SCRIPT_FILENAME'] ) && ( strpos( $_SERVER['SCRIPT_FILENAME'], 'php.cgi' ) == strlen( $_SERVER['SCRIPT_FILENAME'] ) - 7 ) ) {
            $_SERVER['SCRIPT_FILENAME'] = $_SERVER['PATH_TRANSLATED'];
        }
    
        // Fix for Dreamhost and other PHP as CGI hosts.
        if ( strpos( $_SERVER['SCRIPT_NAME'], 'php.cgi' ) !== false ) {
            unset( $_SERVER['PATH_INFO'] );
        }
    
        // Fix empty PHP_SELF.
        $PHP_SELF = $_SERVER['PHP_SELF'];
        if ( empty( $PHP_SELF ) ) {
            $_SERVER['PHP_SELF'] = preg_replace( '/(\?.*)?$/', '', $_SERVER['REQUEST_URI'] );
            $PHP_SELF            = $_SERVER['PHP_SELF'];
        }
    
        $this->wp_populate_basic_auth_from_authorization_header();
    }

    private function wp_populate_basic_auth_from_authorization_header() {
        // If we don't have anything to pull from, return early.
        if ( ! isset( $_SERVER['HTTP_AUTHORIZATION'] ) && ! isset( $_SERVER['REDIRECT_HTTP_AUTHORIZATION'] ) ) {
            return;
        }
    
        // If either PHP_AUTH key is already set, do nothing.
        if ( isset( $_SERVER['PHP_AUTH_USER'] ) || isset( $_SERVER['PHP_AUTH_PW'] ) ) {
            return;
        }
    
        // From our prior conditional, one of these must be set.
        $header = isset( $_SERVER['HTTP_AUTHORIZATION'] ) ? $_SERVER['HTTP_AUTHORIZATION'] : $_SERVER['REDIRECT_HTTP_AUTHORIZATION'];
    
        // Test to make sure the pattern matches expected.
        if ( ! preg_match( '%^Basic [a-z\d/+]*={0,2}$%i', $header ) ) {
            return;
        }
    
        // Removing `Basic ` the token would start six characters in.
        $token    = substr( $header, 6 );
        $userpass = base64_decode( $token );
    
        list( $user, $pass ) = explode( ':', $userpass );
    
        // Now shove them in the proper keys where we're expecting later on.
        $_SERVER['PHP_AUTH_USER'] = $user;
        $_SERVER['PHP_AUTH_PW']   = $pass;
    }

    private function wp_guess_url() {
        if ( defined( 'WP_SITEURL' ) && '' !== WP_SITEURL ) {
            $url = WP_SITEURL;
        } else {
            $abspath_fix         = str_replace( '\\', '/', ABSPATH );
            $script_filename_dir = dirname( $_SERVER['SCRIPT_FILENAME'] );
    
            // The request is for the admin.
            if ( strpos( $_SERVER['REQUEST_URI'], 'gestion' ) !== false || strpos( $_SERVER['REQUEST_URI'], 'wp-login.php' ) !== false ) {
                $path = preg_replace( '#/(gestion/.*|wp-login.php)#i', '', $_SERVER['REQUEST_URI'] );
    
                // The request is for a file in ABSPATH.
            } elseif ( $script_filename_dir . '/' === $abspath_fix ) {
                // Strip off any file/query params in the path.
                $path = preg_replace( '#/[^/]*$#i', '', $_SERVER['PHP_SELF'] );
    
            } else {
                if ( false !== strpos( $_SERVER['SCRIPT_FILENAME'], $abspath_fix ) ) {
                    // Request is hitting a file inside ABSPATH.
                    $directory = str_replace( ABSPATH, '', $script_filename_dir );
                    // Strip off the subdirectory, and any file/query params.
                    $path = preg_replace( '#/' . preg_quote( $directory, '#' ) . '/[^/]*$#i', '', $_SERVER['REQUEST_URI'] );
                } elseif ( false !== strpos( $abspath_fix, $script_filename_dir ) ) {
                    // Request is hitting a file above ABSPATH.
                    $subdirectory = substr( $abspath_fix, strpos( $abspath_fix, $script_filename_dir ) + strlen( $script_filename_dir ) );
                    // Strip off any file/query params from the path, appending the subdirectory to the installation.
                    $path = preg_replace( '#/[^/]*$#i', '', $_SERVER['REQUEST_URI'] ) . $subdirectory;
                } else {
                    $path = $_SERVER['REQUEST_URI'];
                }
            }
    
         //   $schema = $this->is_ssl() ? 'https://' : 'http://'; // set_url_scheme() is not defined yet.
         //   $url    = $schema . $_SERVER['HTTP_HOST'] . $path;
        
        }
    
        return rtrim( $url, '/' );
    }
   

}
$load = new Load();
 echo var_dump('metodo config ', $load->funciones(). '/n'); 
 


?>